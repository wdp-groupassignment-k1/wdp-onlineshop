# WDP Group Assignment K1

[![pipeline status](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/badges/master/pipeline.svg)](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/commits/master)
[![coverage report](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/badges/master/coverage.svg)](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/-/commits/master)

Hi! This is a repository for group **K1** web design and programming course.

## Contents
1. [Group Member](#group-member)
2. [Links to the project](#links-to-the-project)
3. [How to run this project](#how-to-run-this-project)
4. [Project Description](#description)

## Group Member

1. Akbar Putra Novial - 1806173595
2. Muhammad Farhan Oktavian - 1806173512
3. Inez Zahra Nabila - 1806241091
4. Muhammad Ivan Taftazani - 1806241122

## Links to the project
- [GitLab Repository]([https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop)) - ```https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop```
- [Live project on Heroku]([http://freone.herokuapp.com/](http://freone.herokuapp.com/)) - ```http://freone.herokuapp.com/```

## How to run this project

1. Install Python (3.6.5 preferably)
2. Fork or clone this repository.
3. Create a python virtual environment by executing this command:
	```python3 -m venv projectenv```
	>**python3** can be replaced by python according to your installations, **projectenv** can also be replaced by any other name for the virtual environment.
4. Enter and run your virtual environment:
	```cd projectenv```
	```./Scripts/activate```
6. Go to any directory and install pip to your python by executing these commands:
	```curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py```
	```python3 get-pip.py```
7. Navigate to the directory of the cloned repository on your local machine.
8. Install the dependencies used by this project:
	```python3 -m pip install -r requirements.txt```
9. Migrate the database to your local machine:
	```python3 manage.py makemigrations```
	```python3 manage.py migrate```
10. Run the project:
	```python3 manage.py runserver```
11. Access the webpage via browser:
	```localhost:8000```

## Description
>**freoné** is a brand designed to cater fashion needs for both Men and Women. Based in Indonesia, we aim to provide high quality products with reasonable price.

### Website Requirements
#
1. **Items** is each product sold by freoné that are available to purchase through the website. Attributes of item are:
	- Category
	- Name
	- Price
	- Stock
	- Description  

#
2. **Transaction** is the collection of which item that the user had bought with the sum of price minus discount if the user chose to apply coupon(s). The date is also recorded as a reference. Attributes of transaction are:
	- Buyer's Name
	- Item
	- Coupon
	- Total Transaction
	- Date of Purchase  

#
3. **Category** is a class in which an item belongs. Attribute of transaction is:
	- Name  

#
4. **Coupon** is a code that can be applied to a transaction for applying discount. Attributes of Coupon are:
	- Code
	- Discount Percentage
	- Expired Date
	- Minimum Price  

#
5. **Review** is short message that can be written by the website user to make judgement towards a particular item. Attributes of transaction are:
	- Reviewer's Name
	- Star
	- Items
	- Message
	- Date Created  

### Website Features

#
1. **Items** can be searched and viewed from the website. For data entry, each item can be editted and created through Django admin.

#
2. **Transaction** made on the website are saved and displayed on a page that contains all history of transaction.

#
3. **Category** is used to ease the use of the website, items are divided into several categories in which users can pick according to their desired item category.

#
4. **Coupon** available to use on the website are displayed and could be searched by their percentage.

#
5. **Review** can be made towards each item for sale. The review also has a satisfaction level from one up to five.

# WDP Group Assignment K1

[![pipeline status](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/badges/master/pipeline.svg)](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/commits/master)
[![coverage report](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/badges/master/coverage.svg)](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop/-/commits/master)

Hi! This is a repository for group **K1** web design and programming course.

## Contents
1. [Group Member](#group-member)
2. [Links to the project](#links-to-the-project)
3. [How to run this project](#how-to-run-this-project)
4. [Project Description](#description)

## Group Member

1. Akbar Putra Novial - 1806173595
2. Muhammad Farhan Oktavian - 1806173512
3. Inez Zahra Nabila - 1806241091
4. Muhammad Ivan Taftazani - 1806241122

## Links to the project
- [GitLab Repository]([https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop](https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop)) - ```https://gitlab.com/wdp-groupassignment-k1/wdp-onlineshop```
- [Live project on Heroku]([http://freone.herokuapp.com/](http://freone.herokuapp.com/)) - ```http://freone.herokuapp.com/```

## How to run this project

1. Install Python (3.6.5 preferably)
2. Fork or clone this repository.
3. Create a python virtual environment by executing this command:
	```python3 -m venv projectenv```
	>**python3** can be replaced by python according to your installations, **projectenv** can also be replaced by any other name for the virtual environment.
4. Enter and run your virtual environment:
	```cd projectenv```
	```./Scripts/activate```
6. Go to any directory and install pip to your python by executing these commands:
	```curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py```
	```python3 get-pip.py```
7. Navigate to the directory of the cloned repository on your local machine.
8. Install the dependencies used by this project:
	```python3 -m pip install -r requirements.txt```
9. Migrate the database to your local machine:
	```python3 manage.py makemigrations```
	```python3 manage.py migrate```
10. Run the project:
	```python3 manage.py runserver```
11. Access the webpage via browser:
	```localhost:8000```

## Description
>**freoné** is a brand designed to cater fashion needs for both Men and Women. Based in Indonesia, we aim to provide high quality products with reasonable price.

### Website Requirements
#
1. **Items** is each product sold by freoné that are available to purchase through the website. Attributes of item are:
	- Category
	- Name
	- Price
	- Stock
	- Description  

#
2. **Transaction** is the collection of which item that the user had bought with the sum of price minus discount if the user chose to apply coupon(s). The date is also recorded as a reference. Attributes of transaction are:
	- Buyer's Name
	- Item
	- Coupon
	- Total Transaction
	- Date of Purchase  

#
3. **Category** is a class in which an item belongs. Attribute of transaction is:
	- Name  

#
4. **Coupon** is a code that can be applied to a transaction for applying discount. Attributes of Coupon are:
	- Code
	- Discount Percentage
	- Expired Date
	- Minimum Price  

#
5. **Review** is short message that can be written by the website user to make judgement towards a particular item. Attributes of transaction are:
	- Reviewer's Name
	- Star
	- Items
	- Message
	- Date Created  

### Website Features

#
1. **Items** can be searched and viewed from the website. For data entry, each item can be editted and created through Django admin.

#
2. **Transaction** made on the website are saved and displayed on a page that contains all history of transaction.

#
3. **Category** is used to ease the use of the website, items are divided into several categories in which users can pick according to their desired item category.

#
4. **Coupon** available to use on the website are displayed and could be searched by their percentage.

#
5. **Review** can be made towards each item for sale. The review also has a satisfaction level from one up to five.

### Website Requirements 2

#
1. **Authentication** allows user to authenticate towards the platform in two ways, that is registering the account and then logging in and using a google accoount. Upon registration, user has to fill in the required attributes :
	- Username
	- Email
	- Full name
	- Password

the datas that could be seen by users are :
	- Shopping cart
	- Transaction History
	- Reviews

#
2. **Shopping Cart** let users who has been authenticated to add items without refreshing the page, see the content of their shopping cart and get a notification that the item has been added to the cart successfully. On the page where the user can see their items, the user can also reduce the amount of an item or delete the item from the cart quickly and asynchronously.


#
3. **Fast Transactions** allows user to do just one transaction for multiple items. When the transaction is made, the user must be notified of the transaction being made, and whether the transaction is successful or the transaction fails (happens if the stock is out of stock or other errors) quickly without refreshing.

#
4. **Smart Coupons** lets user to find the discount coupon or code in the checkout page easily by typing in the discount coupon code or the amount of discount the user wanted. Then, the user can immediately click on the search results and the coupon that is clicked immediately cuts the total price of purchases.

