$(document).on('click', '#btn-inc', function(){
  var name = $(this).parent().parent().children("#item-name").html();
  var qty = $(this).parent().parent().children("h5").children("#item-qty");
  var price = $(this).parent().parent().children("h3").children("#item-price");
  var total = $('#base-price');
  var count = parseInt(qty.html()) + 1;
  var new_total = parseInt(total.html()) + parseInt(price.html())
  qty.html(count);
  total.html(new_total);
  modify_bag(name, count, new_total, 'modify')
});

$(document).on('click', '#btn-dec', function(){
  var name = $(this).parent().parent().children("#item-name").html();
  var qty = $(this).parent().parent().children("h5").children("#item-qty");
  var price = $(this).parent().parent().children("h3").children("#item-price");
  var total = $('#base-price');
  var count = parseInt(qty.html()) - 1;
  if (count < 0){
    return
  } else {
    var new_total = parseInt(total.html()) - parseInt(price.html())
    qty.html(count);
    total.html(new_total);
  }
  modify_bag(name, count, new_total, 'modify')
});

$(document).on('click', '#btn-rm', function(){
  var e = $(this).parent().parent().parent().parent();
  var name = $(this).parent().parent().children("#item-name").html();
  var qty = $(this).parent().parent().children("h5").children("#item-qty");
  var price = $(this).parent().parent().children("h3").children("#item-price");
  var total = $('#base-price');
  var count = parseInt(qty.html()) * parseInt(price.html());
  var new_total = parseInt(total.html()) - count;
  total.html(new_total);
  e.remove()
  modify_bag(name, 0, new_total, 'remove')
});

function modify_bag(item_name, item_qty, bag_new_price, cmd_type) {
  $.ajax({
    url: `/modify-bag?name=${item_name}&qty=${item_qty}&new-price=${bag_new_price}&type=${cmd_type}`,
    type: 'GET',
    dataType: 'json',
    success: function(res) {
      $("#total-disc").html(0);
      $("#final-amount").html(bag_new_price);
      if (res.empty == "true") {
        setTimeout(function() {
          window.location.reload();
        }, 0);
      }
    }
  });
}

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(document).on('click', '#confirm-checkout', function(){
  var trans_cupon = window.localStorage.getItem('coupon');
  var trans_total = parseInt($("#final-amount").html());
  $.post({
    url: trans_api_url,
    data: {
      cupon: trans_cupon,
      total: trans_total,
    },
    success: function(res) {
      var trans_alert = $("#trans-alert");
      var processing = "Transaction is being processed . . . ";
      var trans_msg = '<div class="alert alert-info" role="alert">' + processing + '</div>';
      trans_alert.html(trans_msg);
      setTimeout(function() {
        if (res.success) {
          trans_msg = '<div class="alert alert-success" role="alert">' + res.transmsg + '</div>';
          trans_alert.html(trans_msg);
          setTimeout(function() {
            window.location = "/transactions/";
          }, 2000);
        } else {
          var trans_msg = '<div class="alert alert-danger" role="alert">' + res.transmsg + '</div>';
          trans_alert.html(trans_msg);
        }
      }, 5000);
    }
  })
});
