from django.test import TestCase, Client
from django.urls import resolve

from .forms import UserRegisterForm

import time

class AuthenticationTest(TestCase):

    def test_register_by_get(self):
        response = self.client.get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_register_post(self):
        form_data = {
            'first_name': "The Hobbit",
            'last_name': "Eissengard",
            'email': "test@test.com",
            'username': "usertest",
            'password1': "SomeRandom123",
            'password2': "SomeRandom123",
        }
        request = self.client.post('/register/', data=form_data)
        self.assertEqual(request.status_code, 302)
