from django.contrib import admin
from onlineshop.models import (
    Category,
    Item,
    Transaction,
    Review,
    TransactionItem,
    )

admin.site.register(Category)
admin.site.register(Item)
admin.site.register(Transaction)
admin.site.register(Review)
admin.site.register(TransactionItem)
