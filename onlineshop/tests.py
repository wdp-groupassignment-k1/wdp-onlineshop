from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.apps import apps
from django.urls import resolve
from datetime import datetime

from .views import index
from .models import Category, Item, Transaction, Review
from cupon.models import Cupon

class ShopTest(TestCase):

    # Test for index page
    def test_index_response(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_using_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


    # Test for category model
    def test_create_model_category(self):
        test_category = Category.objects.create(name="Shirt")
        self.assertEqual(Category.objects.all().count(), 1)

    def test_model_category_return_category_name(self):
        cat_name = "Shirt"
        test_category = Category.objects.create(name = cat_name)
        self.assertEqual(cat_name, str(test_category))

    # Test for item model
    def test_create_model_item(self):
        test_category = Category.objects.create(name="category")
        test_item = Item.objects.create(category=test_category, name="name", price=1, stock=1, description="asd")
        self.assertEqual(Item.objects.all().count(), 1)

    def test_model_item_return_item_name(self):
        item_name = "Black Tee"
        test_category = Category.objects.create(name="category")
        test_item = Item.objects.create(category=test_category, name=item_name, price=1, stock=1, description="asd")
        self.assertEqual(item_name, str(test_item))

    def test_model_item_return_item_price(self):
        item_price = 100000
        test_category = Category.objects.create(name="category")
        test_item = Item.objects.create(category=test_category, name="name", price=item_price, stock=1, description="asd")
        self.assertEqual(item_price, test_item.price)

    def test_model_item_return_item_stock(self):
        item_stock = 10
        test_category = Category.objects.create(name="category")
        test_item = Item.objects.create(category=test_category, name="name", price=1, stock=item_stock, description="asd")
        self.assertEqual(item_stock, test_item.stock)

    def test_model_item_return_item_desc(self):
        item_desc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        test_category = Category.objects.create(name="category")
        test_item = Item.objects.create(category=test_category, name="name", price=1, stock=1, description=item_desc)
        self.assertEqual(item_desc, test_item.description)

    # Test for transaction model
    def test_create_model_transaction(self):
        cupon = Cupon.objects.create(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        mock_user = User.objects.create(username = "username", password = "123poiyth")
        mock_category = Category.objects.create(name="category")
        mock_item = Item.objects.create(category=mock_category, name="name", price=100, stock=2, description="asd")
        test_transaction = Transaction.objects.create(buyer=mock_user, cupon=cupon, total = mock_item.price)
        self.assertEqual(Transaction.objects.all().count(), 1)

    def test_model_transaction_return_transaction_buyer(self):
        cupon = Cupon.objects.create(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        mock_user = User.objects.create(username = "username", password = "123poiyth")
        mock_category = Category.objects.create(name="category")
        mock_item = Item.objects.create(category=mock_category, name="name", price=100, stock=2, description="asd")
        test_transaction = Transaction.objects.create(buyer=mock_user, cupon=cupon, total = mock_item.price)
        self.assertEqual("username", str(test_transaction))

    # Test for review model
    def test_create_model_review(self):
        mock_category = Category.objects.create(name="category")
        mock_item = Item.objects.create(category=mock_category, name="name", price=100, stock=2, description="asd")
        test_review = Review.objects.create(revname="Ivan", star = 3, item = mock_item, message = "Keep up the good work!")
        self.assertEqual(Review.objects.all().count(),1)

    def test_model_review_return_review_name(self):
        rev_name = "Ivan"
        mock_category = Category.objects.create(name="category")
        mock_item = Item.objects.create(category=mock_category, name="name", price=100, stock=2, description="asd")
        test_review = Review.objects.create(revname=rev_name, star = 3, item = mock_item, message = "Keep up the good work!")
        self.assertEqual(rev_name, str(test_review))

    def test_model_review_return_review_star(self):
        rev_star = 5
        mock_category = Category.objects.create(name="category")
        mock_item = Item.objects.create(category=mock_category, name="name", price=100, stock=2, description="asd")
        test_review = Review.objects.create(revname="Ivan", star = rev_star, item = mock_item, message = "Keep up the good work!")
        self.assertEqual(rev_star, test_review.star)

    def test_model_review_return_review_message(self):
        rev_msg = "Ganbatte!"
        mock_category = Category.objects.create(name="category")
        mock_item = Item.objects.create(category=mock_category, name="name", price=100, stock=2, description="asd")
        test_review = Review.objects.create(revname="Ivan", star = 3, item = mock_item, message = rev_msg)
        self.assertEqual(rev_msg, test_review.message)

    def test_add_review_by_get(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        request = self.client.get('/product/test/add-review/', follow=True)
        self.assertEqual(request.status_code, 200)

    def test_add_review_by_post(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        post_data = {
            'revname': 'bagus',
            'revmsg': 'halo',
            'star': 5,
        }
        request = self.client.post('/product/test/add-review/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_view_products_in_a_category(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item1 = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        test_item1 = Item.objects.create(category=test_category, name="eman", slug="tset", price=10, stock=50, description="fgh")
        request = self.client.get('/category/test_cat/', follow=True)
        self.assertEqual(request.status_code, 200)


# Test function that requires authentication
class OnlineShopAuthTestCase(TestCase):

    def setUp(self):
        User.objects.create_superuser('admin', 'foo@foo.com', 'admin')
        self.client.login(username='admin', password='admin')
        self.session = self.client.session

    def test_new_transaction_valid_with_cupon(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        test_coupon = Cupon.objects.create(code="MANTAP", discount = 0.4, expired = datetime.now(), minimum = 10000)
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'total': 500000,
            'cupon': 'MANTAP',
        }
        request = self.client.post('/add-transaction/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_new_transaction_valid_without_cupon(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        test_coupon = Cupon.objects.create(code="MANTAP", discount = 0.4, expired = datetime.now(), minimum = 10000)
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'total': 500000,
            'cupon': '',
        }
        request = self.client.post('/add-transaction/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_new_transaction_not_enough_stock(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=1, description="asd")
        test_coupon = Cupon.objects.create(code="MANTAP", discount = 0.4, expired = datetime.now(), minimum = 10000)
        self.session['cart'] = [('test', 4)]
        self.session.save()
        post_data = {
            'total': 500000,
            'cupon': '',
        }
        request = self.client.post('/add-transaction/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_access_created_transaction(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        test_coupon = Cupon.objects.create(code="MANTAP", discount = 0.4, expired = datetime.now(), minimum = 10000)
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'total': 500000,
            'cupon': 'MANTAP',
        }
        self.client.post('/add-transaction/', data=post_data)
        request = self.client.get('/transactions', follow=True)
        self.assertEqual(request.status_code, 200)
