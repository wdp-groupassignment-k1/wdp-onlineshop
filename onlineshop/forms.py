from django import forms
from onlineshop.models import (
    Category,
    Item,
    Transaction,
    Review,
)
from cupon.models import Cupon

class TransactionForm(forms.ModelForm):

    class Meta:
        model = Transaction
        fields = ('buyer', 'cupon', 'total')
