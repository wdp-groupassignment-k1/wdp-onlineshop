from django.db import models
from cupon.models import Cupon
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length=50, null=False)
    slug = models.SlugField(max_length=70, unique=True)

    def __str__(self):
        return self.name

class Item(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    stock = models.IntegerField()
    description = models.TextField()
    slug = models.SlugField(max_length=70, unique=True)

    def __str__(self):
        return self.name

class Transaction(models.Model):
    buyer = models.ForeignKey(User, on_delete=models.CASCADE)
    cupon = models.ForeignKey(Cupon, on_delete=models.CASCADE, null=True)
    total =  models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.buyer.username

class Review(models.Model):
    revname = models.CharField(max_length = 50, null = False)
    star = models.IntegerField()
    item = models.ForeignKey(Item, on_delete = models.CASCADE)
    message = models.CharField(max_length = 300, null =False)
    datecreated = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.revname

class TransactionItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, null=False)
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE, null=False)
    quantity = models.IntegerField(default=1)
