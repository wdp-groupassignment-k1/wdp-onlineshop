from django.shortcuts import (
    render,
    HttpResponse,
    get_object_or_404,
    redirect,
)
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views import generic
from onlineshop.models import (
    Category,
    Item,
    TransactionItem,
    Transaction,
    Review,
)
from cupon.models import Cupon
from .forms import (
    TransactionForm
)

def index(request):
    category_all = Category.objects.all()
    response = {
        'categories': category_all,
    }
    return render(request, 'index.html', response)

@login_required
def trans(request):
    trans = ()
    transaction = Transaction.objects.filter(buyer = request.user)
    for t in transaction:
        items = ()
        for ti in TransactionItem.objects.filter(transaction=t):
            items += ((ti.item.name, ti.quantity),)
        trans += ((t, items),)
    categories = Category.objects.all()
    response = {
        'transactions': trans,
        'categories': categories,
    }
    return render(request, 'transactions.html', response)

class CatProductList(generic.ListView):
    template_name = 'productlist.html'
    context_object_name = 'items'

    def get_queryset(self):
        cat = get_object_or_404(Category, slug=self.kwargs['slug'])
        return Item.objects.filter(category = cat)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = get_object_or_404(Category, slug=self.kwargs['slug'])
        context['categories'] = Category.objects.all()
        return context

class ItemDetail(generic.DetailView):
    model = Item
    template_name = 'product.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        this_item = get_object_or_404(Item, slug=self.kwargs.get('slug'))
        context['reviews'] = Review.objects.filter(item=this_item.id)
        context['categories'] = Category.objects.all()
        return context

def add_review(request, *args, **kwargs):
    this_item = Item.objects.get(slug=kwargs.get('slug'))
    if request.method == 'POST':
        rev_name = request.POST.get('revname', None)
        rev_msg = request.POST.get('revmsg', None)
        rev_star = int(request.POST.get('star', None))
        new_rev = Review(revname=rev_name, star=rev_star, item=this_item, message=rev_msg)
        new_rev.save()
        return redirect('onlineshop:item', slug=this_item.slug)
    return render(request, 'index.html')

@login_required
def add_to_transaction(request):
    if request.method == 'POST':
        user = request.user
        total = request.POST['total']
        try:
            coupon = Cupon.objects.get(code=request.POST['cupon'])
            new_transaction = Transaction.objects.create(buyer=user, cupon=coupon, total=total)
        except:
            coupon = 0
            new_transaction = Transaction.objects.create(buyer=user, total=total)
        items = []
        for i in request.session['cart']:
            item = Item.objects.get(slug=i[0])
            if (item.stock < i[1]):
                new_transaction.delete()
                json = {
                    "success": False,
                    "transmsg": "There are {} {} left in stock!".format(item.stock, item.name),
                }
                return JsonResponse(json, safe=False)
            items.append((item, i[1]))
        for i in items:
            TransactionItem.objects.create(item=i[0], transaction=new_transaction, quantity=i[1])
            i[0].stock -= i[1]
            i[0].save()
        request.session['cart'] = []
        json = {
            "success": True,
            "transmsg": "Transaction has been successful!",
        }
        return JsonResponse(json, safe=False)
