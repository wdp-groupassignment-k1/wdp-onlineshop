from . import views
from django.urls import path, include
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', views.index, name='index'),
    path('product/<slug>/', views.ItemDetail.as_view(), name='item'),
    path('product/<slug>/add-review/', views.add_review, name='add-review'),
    path('category/<slug>/', views.CatProductList.as_view(), name='cat_product'),
    path('add-transaction/', views.add_to_transaction, name='add-transaction'),
    path('transactions/', views.trans, name = 'transactions'),
]
