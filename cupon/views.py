from django.shortcuts import render
from .models import Cupon
from django.http import (
    JsonResponse,
    HttpResponse,
    HttpResponseBadRequest,
)
from django.core import serializers

def cupon_list(request):
    cupons = Cupon.objects.all()
    response = {
        'cupons': cupons,
    }
    return render(request, 'cupon-list.html', response)

def cupon_min(request, minimum):
    minimum = int(minimum) / 100
    cupons = Cupon.objects.filter(discount__gte=minimum)
    response = {
        'cupons': cupons,
    }
    return render(request, 'cupon-list.html', response)

def search(request):
    if request.method == 'POST':
        search_text = request.POST.get('text_search', None)
        option = request.POST.get('options', None)
        try:
            return cupon_min(request, int(search_text))
        except:
            return cupon_list(request)
    return render(request, 'index.html')

def get_cupon_minim(request):
    spending = request.GET['q']
    cupons = Cupon.objects.filter(minimum__lte=spending)
    response = serializers.serialize("json", cupons),
    return HttpResponse(response, content_type='application/json')

def get_cupons(request):
    name = request.GET['type']
    disc = request.GET['q']
    cupons = {}
    if (name == "code"):
        cupons = Cupon.objects.filter(code__contains=disc)
    elif (name == "discount"):
        cupons = Cupon.objects.filter(discount__gte=int(disc)/100)
    else:
        print("error?")
        return HttpResponse("error", content_type='application/json')
    response = serializers.serialize("json", cupons),
    return HttpResponse(response, content_type='application/json')
