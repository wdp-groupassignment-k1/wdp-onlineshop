from django import forms
from .models import Cupon
from datetime import datetime 

class CuponForm(forms.ModelForm):
    code = forms.CharField(widget=forms.TextInput, required=True)
    discount = forms.FloatField(required=True, min_value=0.001)
    expired = forms.DateTimeField(required = True)
    minimum = forms.IntegerField(required = True)

    class Meta:
        model = Cupon
        fields = ['code', 'discount', 'expired', 'minimum']
        widgets = {'status': forms.TextInput(), 'discount': forms.NumberInput(), 'expired': forms.DateInput(), 'minimum':forms.NumberInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
