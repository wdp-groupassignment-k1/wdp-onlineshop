from . import views
from django.urls import path, include

urlpatterns = [
    path('cupon-list/', views.cupon_list, name='cupon-list'),
    path('cupon-list/<minimum>/', views.cupon_min, name='cupon-min'),
    path('search/', views.search, name='search'),
    path('get-cupon-list/', views.get_cupon_minim, name='minimum-spending'),
    path('get-cupons/', views.get_cupons, name='iterating-all-cupons'),
]
