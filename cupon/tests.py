from django.test import TestCase, Client
from cupon.models import Cupon
from datetime import datetime
from django.urls import resolve
from cupon.forms import CuponForm

class CuponTestCase(TestCase):
    def test_create_cupon(self):
        cupon = Cupon(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        cupon.save()
        count = Cupon.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(cupon.get_percent(), 40)

    def test_form_validation_for_blank_code(self):
        form = CuponForm(data={'code':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['code'],
            ['This field is required.']
        )

    def test_form_validation_for_blank_discount(self):
        form = CuponForm(data={'discount':None})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['discount'],
            ['This field is required.']
        )

    def test_form_validation_for_blank_expired(self):
        form = CuponForm(data={'expired':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['expired'],
            ['This field is required.']
        )


    def test_form_validation_for_blank_minimum(self):
        form = CuponForm(data={'minimum':None})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['minimum'],
            ['This field is required.']
        )

    def test_model_cupon_return_cupon_code(self):
        cupon_code = "MAJUTERUS"
        cupon = Cupon.objects.create(code = cupon_code, discount = 0.4, expired = datetime.now(), minimum = 10000)
        self.assertEqual(cupon_code, str(cupon))

    # Additional test for cupon views
    def test_cupon_list_by_get(self):
        response = self.client.get('/cupon-list/')
        self.assertTemplateUsed(response, 'cupon-list.html')

    def test_cupon_list_by_get_with_query(self):
        Cupon.objects.create(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        response = self.client.get('/cupon-list/30', follow=True)
        self.assertTemplateUsed(response, 'cupon-list.html')
        self.assertEqual(response.status_code, 200)

    def test_cupon_search_by_get(self):
        response = self.client.get('/search/')
        self.assertTemplateUsed(response, 'index.html')

    def test_cupon_search_by_post_success(self):
        Cupon.objects.create(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        post_data = {
            'text_search': "30",
            'options': 'coupon',
        }
        response = self.client.post('/search/', data=post_data, follow=True)
        self.assertTemplateUsed(response, 'cupon-list.html')
        self.assertEqual(response.status_code, 200)

    def test_cupon_search_by_post_fail(self):
        Cupon.objects.create(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        post_data = {
            'text_search': "xa-12",
            'options': 'coupon',
        }
        response = self.client.post('/search/', data=post_data, follow=True)
        self.assertTemplateUsed(response, 'cupon-list.html')
        self.assertEqual(response.status_code, 200)

    def test_cupon_minimum_api(self):
        Cupon.objects.create(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        get_data = {
            'q': 100000,
        }
        response = self.client.get('/get-cupon-list/', data=get_data, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_cupon_general_api(self):
        Cupon.objects.create(code = "GRABAMANOVO",discount = 0.4, expired = datetime.now(), minimum = 10000)
        get_data = {
            'q': 'GRAB',
            'type': 'code'
        }
        response = self.client.get('/get-cupons/', data=get_data, follow=True)
        self.assertEqual(response.status_code, 200)
        get_data = {
            'q': 10,
            'type': 'discount'
        }
        response = self.client.get('/get-cupons/', data=get_data, follow=True)
        self.assertEqual(response.status_code, 200)
        get_data = {
            'q': 100000,
            'type': 'lol'
        }
        response = self.client.get('/get-cupons/', data=get_data, follow=True)
        self.assertEqual(response.status_code, 200)
