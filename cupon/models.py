from django.db import models

class Cupon(models.Model):
    code = models.CharField(max_length=18, null=False)
    discount = models.FloatField()
    expired = models.DateTimeField()
    minimum = models.PositiveIntegerField()

    def __str__(self):
        return self.code

    def get_percent(self):
        return self.discount * 100
