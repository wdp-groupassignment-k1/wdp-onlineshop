from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from onlineshop.models import (
    Category,
    Item,
)

@login_required
def add_to_bag(request):
    if request.method == 'POST':
        this_item = request.POST['slug']
        try:
            if 'cart' in request.session:
                cart = request.session['cart']
                item_exist = False
                for item in cart:
                    if item[0] == this_item:
                        item[1] += 1
                        item_exist = True
                if not item_exist:
                    cart.append((this_item, 1))
                request.session['cart'] = cart
            else:
                request.session['cart'] = []
                cart = request.session['cart']
                cart.append((this_item, 1))
                request.session['cart'] = cart
            json = {
                "success": True,
                "bagmsg": "Successfully added to shopping bag!",
            }
            return JsonResponse(json, safe=False)
        except:
            json = {
                "success": False,
                "bagmsg": "An error occured!",
            }
            return JsonResponse(json, safe=False)

@login_required
def shopping_bag(request):
    items = []
    if 'cart' in request.session.keys():
        cart = request.session['cart']
        for c in cart:
            items.append((Item.objects.get(slug=c[0]), c[1]))
    else:
        response = {
            'empty': True,
            'items': None,
            'total': 0,
            'categories': Category.objects.all(),
        }
        return render(request, "shopping-bag.html", response)
    total = 0
    for item in items:
        total += item[0].price * item[1]
    request.session['base_price'] = total
    response = {
        'empty': False,
        'items': items,
        'total': total,
        'categories': Category.objects.all(),
    }
    return render(request, "shopping-bag.html", response)

def modify_bag(request):
    item = Item.objects.get(name=request.GET['name']).slug
    qty = request.GET['qty']
    new_total = request.GET['new-price']
    type = request.GET['type']
    cart = request.session['cart']
    if type == 'remove':
        for c in cart:
            if c[0] == item:
                cart.remove(c)
    else:
        for c in cart:
            if c[0] == item:
                c[1] = int(qty)
    request.session['base_price'] = new_total
    json = {
        "item": item,
        "qty": qty,
        "new_price": new_total,
        "type": type,
    }
    if not cart:
        json["empty"] = "true"
    else:
        json["empty"] = "false"
    return JsonResponse(json, safe=False)
