from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from onlineshop.models import Category, Item

import json

class ShoppingBagTestCase(TestCase):

    def setUp(self):
        User.objects.create_superuser('admin', 'foo@foo.com', 'admin')
        self.client.login(username='admin', password='admin')
        self.session = self.client.session

    def test_add_to_bag_initial(self):
        post_data = {
            'slug': 'test',
        }
        request = self.client.post('/add-to-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_add_to_bag_cart_exist(self):
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'slug': 'test',
        }
        request = self.client.post('/add-to-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_add_to_bag_cart_not_exist(self):
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'slug': 'polo',
        }
        request = self.client.post('/add-to-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_add_to_bag_cart_invalid(self):
        self.session['cart'] = [('test', 'a')]
        self.session.save()
        post_data = {
            'slug': 'test',
        }
        request = self.client.post('/add-to-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_shopping_bag_exist(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=1, description="asd")
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'slug': 'test',
        }
        request = self.client.post('/shopping-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_shopping_bag_non_existent(self):
        post_data = {
            'slug': 'test',
        }
        request = self.client.post('/shopping-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_modify_shopping_bag_success(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'name': 'name',
            'qty': 2,
            'new-price': 100000,
            'type': 'modify',
        }
        request = self.client.get('/modify-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)

    def test_modify_shopping_bag_remove(self):
        test_category = Category.objects.create(name="category", slug="test_cat")
        test_item = Item.objects.create(category=test_category, name="name", slug="test", price=1, stock=100, description="asd")
        self.session['cart'] = [('test', 1)]
        self.session.save()
        post_data = {
            'name': 'name',
            'qty': 1,
            'new-price': 100000,
            'type': 'remove',
        }
        request = self.client.get('/modify-bag/', data=post_data, follow=True)
        self.assertEqual(request.status_code, 200)
