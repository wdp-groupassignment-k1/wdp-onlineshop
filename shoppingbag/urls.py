from . import views
from django.urls import path, include

urlpatterns = [
    path('add-to-bag/', views.add_to_bag, name='add-bag'),
    path('shopping-bag/', views.shopping_bag, name='shopping-bag'),
    path('modify-bag/', views.modify_bag, name='modify-bag'),
]
