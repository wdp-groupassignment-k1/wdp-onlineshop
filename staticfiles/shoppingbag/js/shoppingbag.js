
$(document).ready(function(){
  console.log("test")
  $("#submit-button").click(function() {
    let price = parseFloat($("#base-price").html());
    window.localStorage.setItem('coupon', String($("#coupon-code-input").val()));
    $.ajax({
      url: `/get-cupons?type=code&q=${$("#coupon-code-input").val()}`,
      type: 'GET',
      dataType: 'json',
      success: function(res) {
        if (!res || res.length !== 1) return;
        let discount = res[0].fields.discount
        $("#coupon-code-input").val("")
        $("#total-disc").html(`${price * discount}`);
        $("#final-amount").html(`${price * (1-discount)}`);
      }
    })
  })
  $(document).on("click",".autocomplete-child", function(){
    $("#coupon-code-input").val($(this).text());
    $("#autocomplete-list").remove();
  })
  $( "#coupon-code-input").on("input", function(){
    let type = $("input[name='name']:checked").val();
    let coupon = $(this).val();
    $("#autocomplete-list").remove();
    if(!coupon){return}
    $.ajax({
      url: `/get-cupons?type=${type}&q=${coupon}`,
      type: 'GET',
      dataType: 'json',
      success: function(res) {
        //console.log(res);
        $('#coupon-code-input').after(`
        <div id="autocomplete-list" class="autocomplete-items">
            ${res.map(child => "<div class='autocomplete-child' >" + child.fields.code + "</div>").join(' ')}
        </div>
        `);
        },
      error: function(res) {
        $("#autocomplete-list").remove();
      }
    });
  });
});
